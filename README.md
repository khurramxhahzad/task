## Task

This represents a straightforward execution of the given task, a compact application involving customers and products. While the customer display is basic, the primary emphasis lies in the meticulous design of backend logic, adhering to coding standards and incorporating best practices such as SOLID principles and the Repository Design Pattern.

To set up the project, follow these instructions and commands:

- composer install
- update environment variables in .env 
- php artisan migrate
- php artisan db:seed

Upon successfully running these commands, the project should be up and running.

To run test cases, use the command "php artisan test."
