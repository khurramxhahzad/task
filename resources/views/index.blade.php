<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Customers</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css">
</head>
<body class="container-fluid">
    <div class="row justify-content-md-center mt-3">
        <div class="col-6">
            <table class="table table-hover">
                <tr>
                    <th>Name</th>
                    <th>Email Address</th>
                    <th>Product Amount</th>
                </tr>

                @forelse($customers as $customer)
                    <tr>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->email_address }}</td>
                        <td>{{ $customer->products->count() }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center"> {{ 'Please run seeders to generate fake records' }}</td>
                    </tr>
                @endforelse
            </table>
        </div>
    </div>
    <div class="row justify-content-md-center">
        <div class="col-6">
            {{ $customers->links() }}
        </div>
    </div>

</body>
</html>
