<?php


namespace App\Repository;

use App\Contracts\CustomerContract;
use App\Models\Customer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class CustomerRepository implements CustomerContract
{
    public function paginated(): LengthAwarePaginator
    {
        return Customer::with('products')->paginate(10);
    }
}
