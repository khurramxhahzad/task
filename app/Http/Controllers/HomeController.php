<?php

namespace App\Http\Controllers;

use App\Contracts\CustomerContract;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    public function __construct(protected CustomerContract $customer) {}

    public function index(): Renderable
    {
        return view('index', [
            'customers' => $this->customer->paginated()
        ]);
    }
}
