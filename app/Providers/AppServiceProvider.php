<?php

namespace App\Providers;

use App\Contracts\CustomerContract;
use App\Repository\CustomerRepository;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        Sanctum::ignoreMigrations();

        Schema::defaultStringLength(191);

        // use bootstrap template for pagination
        Paginator::useBootstrapFive();

        $this->app->bind(CustomerContract::class, CustomerRepository::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }
}
