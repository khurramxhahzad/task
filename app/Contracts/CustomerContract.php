<?php

namespace App\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CustomerContract
{
    public function paginated(): LengthAwarePaginator;
}
