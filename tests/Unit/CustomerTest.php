<?php

namespace Tests\Unit;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     */

    public function setUp(): void
    {
        parent::setUp();

        $this->refreshDatabase();
    }

    public function test_customer_has_full_name_attribute()
    {
        $customer = Customer::factory()->create([
            'first_name' => 'Khurram',
            'last_name' => 'Shahzad',
            'email_address' => 'khurramshahzadkhan11@gmail.com',
        ]);

        $this->assertEquals('Khurram Shahzad', $customer->name);
    }

    public function test_customer_can_have_products()
    {
        $customer = Customer::factory()->create();
        $product = Product::factory()->create();

        $customer->products()->attach($product);

        $this->assertTrue($customer->products->contains($product));
    }
}
