<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Database\Seeder;

class CreateCustomersSeeder extends Seeder
{
    public function run()
    {
        Customer::factory(50)->create()->each(function ($customer) {
            $customer->products()->sync($this->randomProducts());
        });
    }

    // generating random products to attach with each customer
    private function randomProducts(): array {
        $products = range(1, Product::count());

        shuffle($products);

        return array_slice($products, 0, rand(1, 15));
    }
}
