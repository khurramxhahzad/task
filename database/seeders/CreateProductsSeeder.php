<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class CreateProductsSeeder extends Seeder
{
    public function run()
    {
        Product::factory(200)->create();
    }
}
